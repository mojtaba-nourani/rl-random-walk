Goal: Estimation the value function based on Temporal difference and monte carlo tree search.

Problem definition:
In this example we empirically compare the prediction abilities of TD(0) and constant-\alpha MC applied to 
a small Markov process. All episodes start in the center state, C, and
proceed either left or right by one state on each step, with equal probability.
This behavior is presumably due to the combined effect of a fixed policy
and an environment's state-transition probabilities, but we do not care which;
we are concerned only with predicting returns however they are generated.
Episodes terminate either on the extreme left or the extreme right.