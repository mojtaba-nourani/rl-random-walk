% Random walk, Mojtaba Nourani, 9523405064
clear;clc;close all;
%% Initialization:
n_Episode = 130;
n_seq = 100;
Ri = randi(2,10000,n_seq);  % suppose max. number of walks in 100 episode is 10000
% TD:
valueTD = ones(n_Episode,5);
valueTD(n_Episode+1,:)= 0.5*ones(1,5);   % Initial Values
valueTD(n_Episode+2,:)= 1/6*(1:5);       % True Values
% MC:
allst=zeros(1,100);
Valuemc= ones(n_Episode,5);
Valuemc(n_Episode+1,:)= 0.5*ones(1,5);
Valuemc(n_Episode+2,:)= 1/6*(1:5);       % True Values

AlphaMC = [0.01,0.02,0.03,0.04];
AlphaTD = [0.05,0.1,0.15];
Alpha = [AlphaMC AlphaTD];
onvanMC=['MC    \alpha = 0.01';'MC    \alpha = 0.02';'MC    \alpha = 0.03';'MC    \alpha = 0.04';];
onvanTD=['TD(0) \alpha = 0.05';'TD(0) \alpha = 0.10';'TD(0) \alpha = 0.15'];
onvan=[onvanMC;onvanTD];
gama = 1;   % this problem is episodic and undiscounted?!
a = [1,-1]; % action: Right=+1 Left=-1
for A=1:length(Alpha)       % Choosing variouse value for alpha
    RMSTD = zeros(n_seq,n_Episode);
    RMSMC = zeros(n_seq,n_Episode);
    alpha = Alpha(A);
    for Se = 1:n_seq
        v = 0.5*ones(1,7);  % for each sequence v is initialized
        v([1,7])=0;
        Vmc = 0.5*ones(1,5);
        walk = 0;   % Number of all walks in all episodes of one sequence
        ri = Ri(:,Se);
        for Ep=1:n_Episode
            i=1;    % Number of all walks in one episode
            p = 4; % initial position (Left=1 A=2, B=3, C=4, D=5, E=6 FinalRight=7)
            allst(i)=p;    % Memory of sates seen
            while (p~=1 && p~=7) % Episodes terminate either on the extreme left or the extreme right.
                i=i+1;
                walk = walk+1;
                pn = p + a(ri(walk));r=0;
                if pn==7
                    r=1; % If episode terminates on the right, a reward of +1 occurs, otherwise all rewards are zero.
                end
                v(p) = v(p)+alpha*(r+gama*v(pn)-v(p));
                p = pn;
                allst(i)=p;
            end
            % for TD(0):
            valueTD(Ep,:) = v(2:6);   % saving final value of each episode
            e = (valueTD(Ep,:)-valueTD(end,:));
            RMSTD(Se,Ep) = sqrt(mean(e.^2));
            % for Monte Carlo:
            for si=1:i-1
                %                 Note that in this problem return is equal to final reward,
                %                 which is zero or one!
                Vmc(allst(si)-1) = Vmc(allst(si)-1) + alpha*(r - Vmc(allst(si)-1));
            end
            Valuemc(Ep,:) =Vmc;
            emc=(Valuemc(end,:)-Valuemc(Ep,:));
            RMSMC(Se,Ep) = sqrt(mean(emc.^2)); % for each episode
        end
    end
    %% Plotting value function after various number of episodes for TD & MC
    temp = [n_Episode+1,1,10,100,n_Episode+2];
    figure;hold all
    for j=1:length(temp)
        if alpha==0.05 || alpha==0.10 || alpha==0.15    %TD
            plot(valueTD(temp(j),:),'--o','LineWidth',2,'MarkerEdgeColor','r',...
                'MarkerFaceColor','y','MarkerSize',10);
        else  %MC
            plot(Valuemc(temp(j),:),'--o','LineWidth',2,'MarkerEdgeColor','r',...
                'MarkerFaceColor','y','MarkerSize',10);
        end
    end
    xlabel('State');ylabel('Estimated Value');
    set(gca,'XTick',1:5)
    set(gca,'XTickLabel',{'A','B','C','D','E'})
    set(gca,'YTick',.2:.2:.8)
    axis([.5,5.5,0,.95])
    title(onvan(A,:));
    legend('Initial Values','First Episode','10th Episode','100th Episode','True Values','Location','SouthEast')
    %% Plotting RMS error for each alpha
    RMSTD_av = mean(RMSTD);
    RMSMC_av=mean(RMSMC);
    figure(100);hold all
    if alpha==0.05 || alpha==0.10 || alpha==0.15
        plot(RMSTD_av);    %TD
    else
        plot(RMSMC_av);     %MC
    end
    axis([0,n_Episode,0,0.3])
end
title('Learning Curves for various values of \alpha for random walk');
ylabel('RMS Errors Averaged Over States');
xlabel('Number of Episodes');
legend('MC    \alpha = 0.01','MC    \alpha = 0.02','MC    \alpha = 0.03','MC    \alpha = 0.04','TD(0) \alpha=0.05','TD(0) \alpha=0.1','TD(0) \alpha=0.15');
% Here I would like to thank Dr.Ejtahed for his valuable helps during
% this course.